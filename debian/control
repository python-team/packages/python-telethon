Source: python-telethon
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 python3-all-dev,
 python3-pyaes <!nocheck>,
 python3-pytest <!nocheck>,
 python3-pytest-asyncio <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-setuptools,
 python3-sphinx,
 python3-sphinx-rtd-theme,
 python3-rsa <!nocheck>,
Standards-Version: 4.6.0
Testsuite: autopkgtest-pkg-python
Section: python
Homepage: https://github.com/LonamiWebs/Telethon
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-telethon
Vcs-Git: https://salsa.debian.org/python-team/packages/python-telethon.git

Package: python3-telethon
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 ${python3:Recommends},
Suggests:
 ${python3:Suggests},
Description: Pure Python 3 MTProto API Telegram client library
 Telethon is an asyncio Python 3 MTProto library to interact with Telegram's
 API as a user or through a bot account (bot API alternative).
 .
 Telegram is a popular messaging application. This library is meant to make it
 easy for you to write Python programs that can interact with Telegram. Think
 of it as a wrapper that has already done the heavy job for you, so you can
 focus on developing an application.

Package: python3-telethon-doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Pure Python 3 MTProto API Telegram client library - documentation
 Telethon is an asyncio Python 3 MTProto library to interact with Telegram's
 API as a user or through a bot account (bot API alternative).
 .
 Telegram is a popular messaging application. This library is meant to make it
 easy for you to write Python programs that can interact with Telegram. Think
 of it as a wrapper that has already done the heavy job for you, so you can
 focus on developing an application.
 .
 This package contains the HTML documentation.
